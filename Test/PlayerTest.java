import CricketMatch.*;
import org.testng.annotations.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PlayerTest {
    @Test
    void shouldReturnSixAsTotalRunsWhenTwoAndFourIsGivenAsRuns()
    {
        NormalBatting normalBatting=new NormalBatting();
        NormalBowling normalBowling=new NormalBowling();
        Player player=new Player(normalBatting,normalBowling);
        player.scoredRun(2);
        player.totalRuns(false);
        player.scoredRun(4);

        int actual= player.totalRuns(false);

        assertEquals(6,actual);
    }

    @Test
    void shouldReturnTrueWhenThePlayerHasChasedTheTarget()
    {
        NormalBatting normalBatting=new NormalBatting();
        NormalBowling normalBowling=new NormalBowling();
        Player player=new Player(normalBatting,normalBowling);
        player.scoredRun(2);
        player.totalRuns(false);
        player.scoredRun(5);
        player.totalRuns(false);
        int target=6;

        boolean actual=player.hasChasedTarget(target);

        assertTrue(actual);
    }
    @Test
    void shouldReturnFalseWhenThePlayerHasNotChasedTheTarget()
    {
        NormalBatting normalBatting=new NormalBatting();
        NormalBowling normalBowling=new NormalBowling();
        Player player=new Player(normalBatting,normalBowling);
        player.scoredRun(2);
        player.totalRuns(false);
        player.scoredRun(3);
        player.totalRuns(false);
        int target=6;

        boolean actual=player.hasChasedTarget(target);

        assertFalse(actual);
    }
    @Test
    void shouldReturnTrueWhenTheNormalBowlerHasTakenWicket()
    {
        NormalBatting normalBatting=new NormalBatting();
        NormalBowling normalBowling=new NormalBowling();
        Player playerOne=new Player(normalBatting,normalBowling);
        Player playerTwo=new Player(normalBatting,normalBowling);
        int bowlerGivenRun=2;
        int batsmanScoredRun=2;
        playerOne.setGivenRun(bowlerGivenRun);

        boolean actual = playerOne.hasTakenWicket(batsmanScoredRun,playerTwo.isTailEnder());

        assertTrue(actual);
    }
    @Test
    void shouldReturnFalseWhenTheNormalBowlerHasNotTakenWicket()
    {
        NormalBatting normalBatting=new NormalBatting();
        NormalBowling normalBowling=new NormalBowling();
        Player playerOne=new Player(normalBatting,normalBowling);
        Player playerTwo=new Player(normalBatting,normalBowling);
        int bowlerGivenRun=2;
        int batsmanScoredRun=3;
        playerOne.setGivenRun(bowlerGivenRun);

        boolean actual = playerOne.hasTakenWicket(batsmanScoredRun,playerTwo.isTailEnder());

        assertFalse(actual);
    }
    @Test
    void shouldReturnTrueWhenTheNormalBowlerHasTakenWicketOfTailEnderBatsman()
    {
        TailEnderBatting tailEnderBatting=new TailEnderBatting();
        NormalBowling normalBowling=new NormalBowling();
        Player playerOne=new Player(tailEnderBatting,normalBowling);
        Player playerTwo=new Player(tailEnderBatting,normalBowling);
        int bowlerGivenRun=4;
        int batsmanScoredRun=2;
        playerOne.setGivenRun(bowlerGivenRun);

        boolean actual = playerOne.hasTakenWicket(batsmanScoredRun,playerTwo.isTailEnder());

        assertTrue(actual);
    }
    @Test
    void shouldReturnFalseWhenTheNormalBowlerHasNotTakenWicketOfTailEnderBatsman()
    {
        TailEnderBatting tailEnderBatting=new TailEnderBatting();
        NormalBowling normalBowling=new NormalBowling();
        Player playerOne=new Player(tailEnderBatting,normalBowling);
        Player playerTwo=new Player(tailEnderBatting,normalBowling);
        int bowlerGivenRun=3;
        int batsmanScoredRun=2;
        playerOne.setGivenRun(bowlerGivenRun);

        boolean actual = playerOne.hasTakenWicket(batsmanScoredRun,playerTwo.isTailEnder());

        assertFalse(actual);
    }
    @Test
    void shouldReturnTrueWhenTheBallBowledByPartTimeBowlerIsDeadBall()
    {
        NormalBatting normalBatting=new NormalBatting();
        PartTimeBowling partTimeBowling=new PartTimeBowling();
        Player playerOne=new Player(normalBatting,partTimeBowling);
        Player playerTwo=new Player(normalBatting,partTimeBowling);
        int bowlerGivenRun=2;
        int batsmanScoredRun=2;
        playerOne.setGivenRun(bowlerGivenRun);

        boolean actual = playerOne.isDeadBall(batsmanScoredRun,playerTwo.isTailEnder());

        assertTrue(actual);
    }
    @Test
    void shouldReturnFalseWhenTheBallBowledByPartTimeBowlerIsNotDeadBall()
    {
        NormalBatting normalBatting=new NormalBatting();
        PartTimeBowling partTimeBowling=new PartTimeBowling();
        Player playerOne=new Player(normalBatting,partTimeBowling);
        Player playerTwo=new Player(normalBatting,partTimeBowling);
        int bowlerGivenRun=3;
        int batsmanScoredRun=2;
        playerOne.setGivenRun(bowlerGivenRun);

        boolean actual = playerOne.isDeadBall(batsmanScoredRun,playerTwo.isTailEnder());

        assertFalse(actual);
    }
    @Test
    void shouldReturnTrueWhenTheBallBowledByPartTimeBowlerToTailEnderBatsmanIsDeadBall()
    {
        TailEnderBatting tailEnderBatting=new TailEnderBatting();
        PartTimeBowling partTimeBowling=new PartTimeBowling();
        Player playerOne=new Player(tailEnderBatting,partTimeBowling);
        Player playerTwo=new Player(tailEnderBatting,partTimeBowling);
        int bowlerGivenRun=4;
        int batsmanScoredRun=2;
        playerOne.setGivenRun(bowlerGivenRun);

        boolean actual = playerOne.isDeadBall(batsmanScoredRun,playerTwo.isTailEnder());

        assertTrue(actual);
    }
    @Test
    void shouldReturnFalseWhenTheBallBowledByPartTimeBowlerToTailEnderBatsmanIsNotDeadBall()
    {
        TailEnderBatting tailEnderBatting=new TailEnderBatting();
        PartTimeBowling partTimeBowling=new PartTimeBowling();
        Player playerOne=new Player(tailEnderBatting,partTimeBowling);
        Player playerTwo=new Player(tailEnderBatting,partTimeBowling);
        int bowlerGivenRun=3;
        int batsmanScoredRun=2;
        playerOne.setGivenRun(bowlerGivenRun);

        boolean actual = playerOne.isDeadBall(batsmanScoredRun,playerTwo.isTailEnder());

        assertFalse(actual);
    }
}
