package Fakes;

import CricketMatch.Batsman;
import CricketMatch.Bowler;
import CricketMatch.Player;

public class MockPlayer extends Player {

    public MockPlayer(Batsman batsman, Bowler bowler) {
        super(batsman, bowler);
    }
}
