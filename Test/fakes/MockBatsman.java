package Fakes;

import CricketMatch.Batsman;

public class MockBatsman extends Batsman {
    private final boolean chasedTarget;
    public int totalRunsInvocations=0;
    public int hasChasedTargetInvocations=0;

    public MockBatsman(boolean chasedTarget,int type) {
        super(type);
        this.chasedTarget = chasedTarget;
    }


    @Override
    public int totalRuns(int run) {
        totalRunsInvocations++;
        return super.totalRuns(run);
    }

    @Override
    public boolean hasChasedTarget(int target) {
        hasChasedTargetInvocations++;
        return chasedTarget;
    }
}
