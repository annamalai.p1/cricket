package Fakes;

import CricketMatch.Bowler;

public class MockBowler extends Bowler {
    public final int type;
    private final boolean wicket;
    public int run;
    public int noOfHasTakenWicketInvocations=0;

    public MockBowler(int type, boolean wicket) {
        super(type);
        this.type = type;
        this.wicket = wicket;
    }

    @Override
    public void setRun(int run) {
        this.run = run;
    }

    @Override
    public boolean hasTakenWicket(int runs,int batsmanType) {
        noOfHasTakenWicketInvocations++;
        return wicket;
    }

}
