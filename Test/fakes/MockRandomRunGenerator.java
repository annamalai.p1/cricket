package Fakes;

import CricketMatch.RandomRunGenerator;

public class MockRandomRunGenerator extends RandomRunGenerator {
    private final int randomRun;
    private final int batsmanRun;
    public int getRunInvocations=0;
    public int getBatsmanRunInvocations=0;


    public MockRandomRunGenerator(int randomRun, int batsmanRun) {
        this.randomRun = randomRun;
        this.batsmanRun = batsmanRun;
    }

    @Override
    public int getRun(int limit) {
        getRunInvocations++;
        return randomRun;
    }

    @Override
    public int getBatsmanRun(int type) {
        getBatsmanRunInvocations++;
        return batsmanRun;
    }
}



