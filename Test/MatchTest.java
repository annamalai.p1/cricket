import CricketMatch.Match;
import Fakes.MockBatsman;
import Fakes.MockBowler;
import Fakes.MockRandomRunGenerator;
import org.testng.annotations.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MatchTest {
    @Test
    public void shouldReturnBatsmanHasWonIfTheNormalBatsmanChasedTheTarget() {
        int overs = 1;
        int target = 3;
        int batsmanType = 0;
        int bowlerType = 0;
        MockBatsman mockBatsman = new MockBatsman(true);
        MockBowler mockBowler = new MockBowler(bowlerType, false);
        MockRandomRunGenerator mockRandomRunGenerator = new MockRandomRunGenerator(3, 4);
        Match match = new Match(mockBatsman, mockBowler, target, overs, batsmanType, bowlerType, mockRandomRunGenerator);
        match.play();

        String actual = match.result();

        assertEquals(Match.BATSMAN_WON, actual);
        assertEquals(3,mockBatsman.hasChasedTargetInvocations);
        assertEquals(1,mockBatsman.totalRunsInvocations);
        assertEquals(1,mockBowler.noOfHasTakenWicketInvocations);
        assertEquals(1,mockRandomRunGenerator.getRunInvocations);
        assertEquals(1,mockRandomRunGenerator.getBatsmanRunInvocations);
    }

    @Test
    public void shouldReturnBatsmanHasLostIfTheNormalBatsmanHasNotChasedTheTarget() {
        int overs = 1;
        int target = 8;
        int batsmanType = 0;
        int bowlerType = 0;
        MockBatsman mockBatsman = new MockBatsman( false);
        MockBowler mockBowler = new MockBowler(bowlerType, true);
        MockRandomRunGenerator mockRandomRunGenerator = new MockRandomRunGenerator(6, 6);
        Match match = new Match(mockBatsman, mockBowler, target, overs, batsmanType, bowlerType, mockRandomRunGenerator);
        match.play();

        String actual = match.result();

        assertEquals(Match.BATSMAN_LOST, actual);
        assertEquals(2,mockBatsman.hasChasedTargetInvocations);
        assertEquals(0,mockBatsman.totalRunsInvocations);
        assertEquals(1,mockBowler.noOfHasTakenWicketInvocations);
        assertEquals(1,mockRandomRunGenerator.getRunInvocations);
        assertEquals(1,mockRandomRunGenerator.getBatsmanRunInvocations);
    }

    @Test
    public void shouldReturnBatsmanHasWonIfTheHitBatsmanChasedTheTarget() {
        int overs = 1;
        int target = 6;
        int batsmanType = 1;
        int bowlerType = 0;
        MockBatsman mockBatsman = new MockBatsman( true);
        MockBowler mockBowler = new MockBowler(bowlerType, false);
        MockRandomRunGenerator mockRandomRunGenerator = new MockRandomRunGenerator(5, 6);
        Match match = new Match(mockBatsman, mockBowler, target, overs, batsmanType, bowlerType, mockRandomRunGenerator);
        match.play();

        String actual = match.result();

        assertEquals(Match.BATSMAN_WON, actual);
        assertEquals(3,mockBatsman.hasChasedTargetInvocations);
        assertEquals(1,mockBatsman.totalRunsInvocations);
        assertEquals(1,mockBowler.noOfHasTakenWicketInvocations);
        assertEquals(1,mockRandomRunGenerator.getRunInvocations);
        assertEquals(1,mockRandomRunGenerator.getBatsmanRunInvocations);
    }

    @Test
    public void shouldReturnBatsmanHasLostIfTheHitBatsmanHasNotChasedTheTarget() {
        int overs = 1;
        int target = 25;
        int batsmanType = 1;
        int bowlerType = 0;
        MockBatsman mockBatsman = new MockBatsman( false);
        MockBowler mockBowler = new MockBowler(bowlerType, false);
        MockRandomRunGenerator mockRandomRunGenerator = new MockRandomRunGenerator(5, 4);
        Match match = new Match(mockBatsman, mockBowler, target, overs, batsmanType, bowlerType, mockRandomRunGenerator);
        match.play();

        String actual = match.result();

        assertEquals(Match.BATSMAN_LOST, actual);
        assertEquals(8,mockBatsman.hasChasedTargetInvocations);
        assertEquals(6,mockBatsman.totalRunsInvocations);
        assertEquals(6,mockBowler.noOfHasTakenWicketInvocations);
        assertEquals(6,mockRandomRunGenerator.getRunInvocations);
        assertEquals(6,mockRandomRunGenerator.getBatsmanRunInvocations);
    }

    @Test
    public void shouldReturnBatsmanHasWonIfTheDefensiveBatsmanChasedTheTarget() {
        int overs = 1;
        int target = 3;
        int batsmanType = 2;
        int bowlerType = 0;
        MockBatsman mockBatsman = new MockBatsman( true);
        MockBowler mockBowler = new MockBowler(bowlerType, false);
        MockRandomRunGenerator mockRandomRunGenerator = new MockRandomRunGenerator(5, 3);
        Match match = new Match(mockBatsman, mockBowler, target, overs, batsmanType, bowlerType, mockRandomRunGenerator);
        match.play();

        String actual = match.result();

        assertEquals(Match.BATSMAN_WON, actual);
        assertEquals(3,mockBatsman.hasChasedTargetInvocations);
        assertEquals(1,mockBatsman.totalRunsInvocations);
        assertEquals(1,mockBowler.noOfHasTakenWicketInvocations);
        assertEquals(1,mockRandomRunGenerator.getRunInvocations);
        assertEquals(1,mockRandomRunGenerator.getBatsmanRunInvocations);
    }

    @Test
    public void shouldReturnBatsmanHasLostIfTheDefensiveBatsmanHasNotChasedTheTarget() {
        int overs = 1;
        int target = 3;
        int batsmanType = 2;
        int bowlerType = 0;
        MockBatsman mockBatsman = new MockBatsman( false);
        MockBowler mockBowler = new MockBowler(bowlerType, true);
        MockRandomRunGenerator mockRandomRunGenerator = new MockRandomRunGenerator(3, 3);
        Match match = new Match(mockBatsman, mockBowler, target, overs, batsmanType, bowlerType, mockRandomRunGenerator);
        match.play();

        String actual = match.result();

        assertEquals(Match.BATSMAN_LOST, actual);
        assertEquals(2,mockBatsman.hasChasedTargetInvocations);
        assertEquals(0,mockBatsman.totalRunsInvocations);
        assertEquals(1,mockBowler.noOfHasTakenWicketInvocations);
        assertEquals(1,mockRandomRunGenerator.getRunInvocations);
        assertEquals(1,mockRandomRunGenerator.getBatsmanRunInvocations);
    }

    @Test
    public void shouldReturnBatsmanHasWonIfTheBatsmanHasChasedTheTargetWhenThePartTimeBowlerBowled() {
        int overs = 1;
        int target = 6;
        int batsmanType = 0;
        int bowlerType = 1;
        MockBatsman mockBatsman = new MockBatsman( true);
        MockBowler mockBowler = new MockBowler(bowlerType, false);
        MockRandomRunGenerator mockRandomRunGenerator = new MockRandomRunGenerator(5, 6);
        Match match = new Match(mockBatsman, mockBowler, target, overs, batsmanType, bowlerType, mockRandomRunGenerator);
        match.play();

        String actual = match.result();

        assertEquals(Match.BATSMAN_WON, actual);
        assertEquals(3,mockBatsman.hasChasedTargetInvocations);
        assertEquals(1,mockBatsman.totalRunsInvocations);
        assertEquals(1,mockBowler.noOfHasTakenWicketInvocations);
        assertEquals(1,mockRandomRunGenerator.getRunInvocations);
        assertEquals(1,mockRandomRunGenerator.getBatsmanRunInvocations);
    }

    @Test
    public void shouldReturnBatsmanHasLostIfTheBatsmanHasNotChasedTheTargetWhenThePartTimeBowlerBowled() {
        int overs = 1;
        int target = 6;
        int batsmanType = 0;
        int bowlerType = 1;
        MockBatsman mockBatsman = new MockBatsman( false);
        MockBowler mockBowler = new MockBowler(bowlerType, false);
        MockRandomRunGenerator mockRandomRunGenerator = new MockRandomRunGenerator(6, 6);
        Match match = new Match(mockBatsman, mockBowler, target, overs, batsmanType, bowlerType, mockRandomRunGenerator);
        match.play();

        String actual = match.result();

        assertEquals(Match.BATSMAN_LOST, actual);
        assertEquals(8,mockBatsman.hasChasedTargetInvocations);
        assertEquals(6,mockBatsman.totalRunsInvocations);
        assertEquals(6,mockBowler.noOfHasTakenWicketInvocations);
        assertEquals(6,mockRandomRunGenerator.getRunInvocations);
        assertEquals(6,mockRandomRunGenerator.getBatsmanRunInvocations);
    }
    @Test
    public void shouldReturnBatsmanHasWonIfTheTailEnderBatsmanHasChasedTheTargetWhenTheNormalBowlerBowled() {
        int overs = 1;
        int target = 4;
        int batsmanType = 3;
        int bowlerType = 0;
        MockBatsman mockBatsman = new MockBatsman( true);
        MockBowler mockBowler = new MockBowler(bowlerType, false);
        MockRandomRunGenerator mockRandomRunGenerator = new MockRandomRunGenerator(5, 6);
        Match match = new Match(mockBatsman, mockBowler, target, overs, batsmanType, bowlerType, mockRandomRunGenerator);
        match.play();

        String actual = match.result();

        assertEquals(Match.BATSMAN_WON, actual);
        assertEquals(3,mockBatsman.hasChasedTargetInvocations);
        assertEquals(1,mockBatsman.totalRunsInvocations);
        assertEquals(1,mockBowler.noOfHasTakenWicketInvocations);
        assertEquals(1,mockRandomRunGenerator.getRunInvocations);
        assertEquals(1,mockRandomRunGenerator.getBatsmanRunInvocations);
    }
    @Test
    public void shouldReturnBatsmanHasLostIfTheTailEnderBatsmanHasNotChasedTheTargetWhenTheNormalBowlerBowled() {
        int overs = 1;
        int target = 6;
        int batsmanType = 3;
        int bowlerType = 0;
        MockBatsman mockBatsman = new MockBatsman( false);
        MockBowler mockBowler = new MockBowler(bowlerType, true);
        MockRandomRunGenerator mockRandomRunGenerator = new MockRandomRunGenerator(4, 6);
        Match match = new Match(mockBatsman, mockBowler, target, overs, batsmanType, bowlerType, mockRandomRunGenerator);
        match.play();

        String actual = match.result();

        assertEquals(Match.BATSMAN_LOST, actual);
        assertEquals(2,mockBatsman.hasChasedTargetInvocations);
        assertEquals(0,mockBatsman.totalRunsInvocations);
        assertEquals(1,mockBowler.noOfHasTakenWicketInvocations);
        assertEquals(1,mockRandomRunGenerator.getRunInvocations);
        assertEquals(1,mockRandomRunGenerator.getBatsmanRunInvocations);
    }
    @Test
    public void shouldReturnBatsmanHasWonIfTheTailEnderBatsmanHasChasedTheTargetWhenThePartTimeBowlerBowled() {
        int overs = 1;
        int target = 4;
        int batsmanType = 3;
        int bowlerType = 1;
        MockBatsman mockBatsman = new MockBatsman( true);
        MockBowler mockBowler = new MockBowler(bowlerType, false);
        MockRandomRunGenerator mockRandomRunGenerator = new MockRandomRunGenerator(5, 6);
        Match match = new Match(mockBatsman, mockBowler, target, overs, batsmanType, bowlerType, mockRandomRunGenerator);
        match.play();

        String actual = match.result();

        assertEquals(Match.BATSMAN_WON, actual);
        assertEquals(3,mockBatsman.hasChasedTargetInvocations);
        assertEquals(1,mockBatsman.totalRunsInvocations);
        assertEquals(1,mockBowler.noOfHasTakenWicketInvocations);
        assertEquals(1,mockRandomRunGenerator.getRunInvocations);
        assertEquals(1,mockRandomRunGenerator.getBatsmanRunInvocations);
    }
    @Test
    public void shouldReturnBatsmanHasLostIfTheTailEnderBatsmanHasNotChasedTheTargetWhenThePartTimeBowlerBowled() {
        int overs = 1;
        int target = 5;
        int batsmanType = 3;
        int bowlerType = 1;
        MockBatsman mockBatsman = new MockBatsman( false);
        MockBowler mockBowler = new MockBowler(bowlerType, false);
        MockRandomRunGenerator mockRandomRunGenerator = new MockRandomRunGenerator(4, 6);
        Match match = new Match(mockBatsman, mockBowler, target, overs, batsmanType, bowlerType, mockRandomRunGenerator);
        match.play();

        String actual = match.result();

        assertEquals(Match.BATSMAN_LOST, actual);
        assertEquals(8,mockBatsman.hasChasedTargetInvocations);
        assertEquals(6,mockBatsman.totalRunsInvocations);
        assertEquals(6,mockBowler.noOfHasTakenWicketInvocations);
        assertEquals(6,mockRandomRunGenerator.getRunInvocations);
        assertEquals(6,mockRandomRunGenerator.getBatsmanRunInvocations);

    }
}