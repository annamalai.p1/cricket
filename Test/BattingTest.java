import CricketMatch.DefensiveBatting;
import CricketMatch.HitBatting;
import CricketMatch.NormalBatting;
import CricketMatch.TailEnderBatting;
import org.testng.annotations.Test;
import static org.junit.jupiter.api.Assertions.*;

public class BattingTest {
    @Test
    void shouldReturnSixAsNormalBatsmanRunWhenSixIsPassedAsIndexToGetRun(){
        NormalBatting normalBatting=new NormalBatting();

        int actual=normalBatting.getRun(6);

        assertEquals(6,actual);
    }
    @Test
    void shouldReturnFourAsTailEnderBatsmanRunWhenFourIsPassedAsIndexToGetRun(){
        TailEnderBatting tailEnderBatting=new TailEnderBatting();

        int actual=tailEnderBatting.getRun(4);

        assertEquals(4,actual);
    }
    @Test
    void shouldReturnZeroAsDefensiveBatsmanRunWhenThreeIsPassedAsIndexToGetRun(){
        DefensiveBatting defensiveBatting=new DefensiveBatting();

        int actual=defensiveBatting.getRun(3);

        assertEquals(0,actual);
    }
    @Test
    void shouldReturnFourAsHitBatsmanRunWhenOneIsPassedAsIndexToGetRun(){
        HitBatting hitBatting=new HitBatting();

        int actual=hitBatting.getRun(1);

        assertEquals(4,actual);
    }
    @Test
    void shouldReturnSixAsHitBatsmanRunWhenSixIsPassedAsIndexToGetRun(){
        HitBatting hitBatting=new HitBatting();

        int actual=hitBatting.getRun(6);

        assertEquals(6,actual);}
}
