package CricketMatch;

public enum BatsmanType {
    NORMAL_BATSMAN(new int[]{0, 1, 2, 3, 4, 5, 6}),
    HIT_BATSMAN(new int[]{0, 4, 6}),
    DEFENSIVE_BATSMAN(new int[]{0, 1, 2, 3}),
    TAIL_ENDER_BATSMAN(new int[]{0, 1, 2, 3, 4, 5, 6});
    public final int[] scores;

    BatsmanType(int[] scores) {
        this.scores = scores;
    }
}
