package CricketMatch;

public class TailEnderBatting implements Batting{
    private final int[] runs=new int[]{0, 1, 2, 3, 4, 5, 6};

    @Override
    public boolean isTailEnder() {
        return true;
    }
    @Override
    public int getRun(int index){
        return runs[index];
    }
}
