package CricketMatch;

public class DefensiveBatting implements Batting{
    private final int[] runs=new int[]{0, 1, 2, 3};

    @Override
    public boolean isTailEnder() {
        return false;
    }

    @Override
    public int getRun(int index) {
        int bound=3;
        index=Math.abs(bound-index);
        return runs[index];
    }
}
