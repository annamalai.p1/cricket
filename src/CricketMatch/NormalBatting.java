package CricketMatch;

public class NormalBatting implements Batting{
    private final int[] runs=new int[]{0, 1, 2, 3, 4, 5, 6};

    @Override
    public boolean isTailEnder() {
        return false;
    }

    @Override
    public int getRun(int index) {
        return runs[index];
    }
}
