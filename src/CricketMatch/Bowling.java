package CricketMatch;

public interface Bowling {
    boolean hasTakenWicket(Boolean isTailEnder,int batsmanRun);
    void setGivenRun(int run);
    boolean isDeadBall(int batsmanRun,boolean isTailEnder);
}
