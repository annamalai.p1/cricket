package CricketMatch;

public class PartTimeBowling implements Bowling {
    private int run;

    @Override
    public boolean hasTakenWicket(Boolean isTailEnder,int batsmanRun ) {
        return false;
    }

    @Override
    public void setGivenRun(int run) {
        this.run=run;
    }

    @Override
    public boolean isDeadBall(int batsmanRun,boolean isTailEnder) {
        if(isTailEnder){
            return (run%2==0 && batsmanRun%2==0) || (run%2==1 && batsmanRun%2==1);
        }
        return run == batsmanRun;    }
}
