package CricketMatch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class StartMatch {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number of overs:");
        int overs = sc.nextInt();
        System.out.println("""
                Types of Batting\s
                   0 : Normal Batman
                   1 : Hit Batsman
                   2 : Defensive Batsman
                   3 : TailEnder Batsman""");
        System.out.println("Enter the Player1 Batting Type:");
        int playerOneBattingType=sc.nextInt();
        System.out.println("Enter the Player2 Batting Type:");
        int playerTwoBattingType=sc.nextInt();
        System.out.println("""
                Types of Bowling\s
                   0 : Normal Bowler
                   1 : Part time Bowler""");
        System.out.println("Enter the Player1 Bowling Type:");
        int playerOneBowlingType=sc.nextInt();
        System.out.println("Enter the Player2 Bowling Type:");
        int playerTwoBowlingType=sc.nextInt();
        Player playerOne=player(playerOneBattingType,playerOneBowlingType);
        Player playerTwo=player(playerTwoBattingType,playerTwoBowlingType);
        RandomRunGenerator randomRunGenerator = new RandomRunGenerator();
        Match match = new Match( overs, randomRunGenerator, playerOne,playerTwo);
        System.out.println(match.play());
    }


    private static Player player(int batsmanType,int bowlerType)
    {
        List<Bowling> bowlingTypes = new ArrayList<>(Arrays.asList(new NormalBowling(),new PartTimeBowling()));
        List<Batting> battingTypes = new ArrayList<>(Arrays.asList(new NormalBatting(),new HitBatting(),new DefensiveBatting(),new TailEnderBatting()));
        return new Player(battingTypes.get(batsmanType),bowlingTypes.get(bowlerType));
    }
}

