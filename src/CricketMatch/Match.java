package CricketMatch;

public class Match {
    public int overs;
    public static final int BALLS_PER_OVER = 6;
    public static final String PLAYER_2_HAS_WON = "Player2 has won";
    public static final String PLAYER_1_HAS_WON = "Player1 has won";
    RandomRunGenerator randomRunGenerator;
    private final Player playerOne;
    private final Player playerTwo;

    public Match(int overs, RandomRunGenerator randomRunGenerator,Player playerOne,Player playerTwo) {
        this.overs = overs;
        this.randomRunGenerator = randomRunGenerator;
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
    }

    public String play() {
        boolean chasingStatus=false;
        int target=0;
        int firstInningsScore= innings(playerOne,playerTwo,chasingStatus,target);
        target=firstInningsScore+1;
        System.out.println("End of InningsOne \n Target="+""+ (target));
        chasingStatus=true;
        int secondInningsRuns=innings(playerTwo,playerOne,chasingStatus,target);
        System.out.println("End of InningsTwo \n Runs="+""+ (secondInningsRuns));
        return result(playerTwo,target);
        }

    private int innings(Player batsman, Player bowler, boolean chasingStatus, int target){
        int runs = 0;
        for (int ball = 0; ball < overs * BALLS_PER_OVER; ball++) {
            int batsmanRun = randomRunGenerator.getBatsmanRun(batsman);
            int bowlerRun = randomRunGenerator.getRun(7);
            bowler.setGivenRun(bowlerRun);
            System.out.println("Batsman1 : " + batsmanRun);
            System.out.println("Bowler2 : " + bowlerRun);
            boolean tailEnderCheck=batsman.isTailEnder();
            boolean wicket = bowler.hasTakenWicket(batsmanRun,tailEnderCheck);
            if (wicket) {
                break;
            }
            boolean deadBallCheck= bowler.isDeadBall(batsmanRun,tailEnderCheck);
            runs=batsman.totalRuns(deadBallCheck);
            if ((chasingStatus) && (batsman.hasChasedTarget(target))) {
                break;
            }
        }
        return runs;
    }



    public String result(Player batsman,int target) {
        if (batsman.hasChasedTarget(target)) {
            return PLAYER_2_HAS_WON;
        }
        return PLAYER_1_HAS_WON;
    }
}





