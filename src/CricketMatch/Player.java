package CricketMatch;

public class Player {
    private final Batting batting;
    private final Bowling bowling;
    private int run;
    private int totalScore;

    public Player(Batting batting, Bowling bowling) {
        this.batting = batting;
        this.bowling = bowling;
    }
    public int scoredRun(int index){
        this.run=batting.getRun(index);
        return run;
    }
    public void setGivenRun(int bowlerRun){
        bowling.setGivenRun(bowlerRun);
    }
    public boolean isDeadBall(int batsmanRun,boolean tailEnderCheck)
    {
        return bowling.isDeadBall(batsmanRun,tailEnderCheck);
    }
    public int totalRuns(boolean deadBallCheck) {
        if(deadBallCheck){
            return totalScore;
        }
        totalScore += this.run;
        return totalScore;
    }
    public boolean hasChasedTarget(int target) {
        return totalScore >= target;
    }
    public boolean hasTakenWicket(int batsmanRun,boolean tailEnderCheck)
    {
        return bowling.hasTakenWicket(tailEnderCheck, batsmanRun);
    }
    public boolean isTailEnder()
    {
        return batting.isTailEnder();
    }


}
