package CricketMatch;

public class NormalBowling implements Bowling{
    private int run;

    @Override
    public void setGivenRun(int run) {
        this.run=run;
    }
    @Override
    public boolean hasTakenWicket( Boolean isTailEnder,int batsmanRun) {
        if(isTailEnder){
            return (run%2==0 && batsmanRun%2==0) || (run%2==1 && batsmanRun%2==1);
        }
        return run == batsmanRun;
    }

    @Override
    public boolean isDeadBall(int batsmanRun,boolean isTailEnder) {
        return false;
    }
}
