package CricketMatch;

import java.util.Random;

public class RandomRunGenerator {
    public int getRun(int limit) {
        Random random = new Random();
        return random.nextInt(limit);
    }

    public int getBatsmanRun(Player player) {
        int bound=7;
        int index=getRun(bound);
        return player.scoredRun(index);
    }
}
