package CricketMatch;

public class Batsman {
    public int type;

    public Batsman(int type) {
        this.type = type;
    }

    public int totalScore = 0;

    public int totalRuns(int run) {
        totalScore += run;
        return totalScore;
    }

    public boolean hasChasedTarget(int target) {
        return totalScore >= target;
    }
}
