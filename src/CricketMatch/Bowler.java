package CricketMatch;

public class Bowler {
    public final int type;
    public int run;

    public Bowler(int type) {
        this.type = type;
    }

    public void setRun(int run) {
        this.run = run;
    }

    public boolean hasTakenWicket(int batsmanRun,int batsmanType) {
        int TAIL_ENDER_BATSMAN=3;
        if(batsmanType==TAIL_ENDER_BATSMAN){
            return (run%2==0 && batsmanRun%2==0) || (run%2!=0 && batsmanRun%2!=0);
        }
            return run == batsmanRun;
    }
}
