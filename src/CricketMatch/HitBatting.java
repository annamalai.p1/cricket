package CricketMatch;

public class HitBatting implements Batting{
    private final int[] runs=new int[]{0, 4, 6};

    @Override
    public boolean isTailEnder() {
        return false;
    }

    @Override
    public int getRun(int index) {
        if(index<=2){
            return runs[index];
        }
        int bound=4;
        index=Math.abs(bound-index);
        return runs[index];
    }
}
