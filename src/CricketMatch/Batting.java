package CricketMatch;

public interface Batting {

    boolean isTailEnder();

    int getRun(int index);
}
